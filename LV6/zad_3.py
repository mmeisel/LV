import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.datasets import load_digits
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.cross_validation import train_test_split 
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import scale
 
# ucitaj podatke
digits = load_digits() 
 
 
# prikaži osnovne informacije o skupu podataka 
print(digits.DESCR) 
 
# ucitaj ulazne velicine (slike) u matricu X dimenzije 1797 sa 64
# (svaka 8x8 slika je razvucen u polje duzine 64 gdje je element vektora jedan pixel na slici)
# odogvarajuca klasa svake slike nalazi se u polju y (dimenzije 1797 x 1)
X, y, images = digits.data, digits.target, digits.images 
 
# 80% slika uzmi za učenje modela, 20% za testiranje
X_train, X_test, y_train, y_test, images_train, images_test = train_test_split(
                                         X, y, images, test_size=0.20, random_state=67)
testS=scale(X_test)
trainS=scale(X_train)
 
# izgradi model logističke regresije na podacima za učenje
LogRegModel = LogisticRegression()
LogRegModel.fit(trainS, y_train) 
 
# pomoću modela procijeni klasu svake slike iz testnog skupa
y_test_predict = LogRegModel.predict(testS) 
 
# usporedi procjenu za svaku sliku testnog skupa sa stvarnom klasom
print("Vrjednovanje klasifikatora %s:\n%s\n"
      % (LogRegModel, classification_report(y_test, y_test_predict)))
print("Matrica zabune:\n%s" % confusion_matrix(y_test, y_test_predict)) 
 
# prikaži četiri slike testnog skupa i procjenu klasifikatora
fig = plt.figure()
n_samples = len(digits.images)
images_and_predictions = list(zip(images_test, y_test_predict))
for index, (image, prediction) in enumerate(images_and_predictions[:4]):
     plt.subplot(2, 4, index + 5)
     plt.axis('off')
     plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
     plt.title('Prediction: %i' % prediction) 
 
plt.show()

ANN=MLPClassifier(hidden_layer_sizes=(10,50), max_iter=1000, activation='relu')
ANN.fit(trainS,y_train)
ANN_ypredict=ANN.predict(testS)
print("Vrjednovanje klasifikatora %s:\n%s\n"
      % (ANN, classification_report(y_test, ANN_ypredict)))
print("Matrica zabune:\n%s" % confusion_matrix(y_test, ANN_ypredict)) 

fig = plt.figure()
n_samples = len(digits.images)
images_and_predictions = list(zip(images_test, ANN_ypredict))
for index, (image, prediction) in enumerate(images_and_predictions[:4]):
     plt.subplot(2, 4, index + 5)
     plt.axis('off')
     plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
     plt.title('Prediction: %i' % prediction) 
 
plt.show()