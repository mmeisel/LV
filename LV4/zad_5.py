import numpy as np
import sklearn.linear_model as lm
import matplotlib.pyplot as plt
def non_func(x): #funkcija koja vraca y listu koja sadrzi stvarne vrijednosti
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
def add_noise(y): #funkcija koja na osnovi y_true vraca stvarne vrijednosti na koje je dodan sum/pogreska
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

def residuals(x,y,t0,t1):
    e=y-(t0+t1*x) #racunanje reziduala
    return e
x = np.linspace(1,10,100)
y_true = non_func(x) #stvarna funkcija koja se treba dobiti sa SU
y_measured = add_noise(y_true) #gneriranje ulaznog signala koji se mjeri; signal je dobiven iz stvarne funkcije dodavanjem suma
np.random.seed(12)
indeksi = np.random.permutation(len(x)) #nasumicna permutacija brojeva do len(x)
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))] #prvih 70% indeksa su indeksi trening skupa
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)] #slijedecih 30% su indexi testnog skupa
x = x[:, np.newaxis] #transponiranje liste x koja je linspace
y_measured = y_measured[:, np.newaxis] #transponira listu mjerenja
xtrain = x[indeksi_train]           #iz varijable x izvlaci se prvih 70% mjerenja koje su trening skup
ytrain = y_measured[indeksi_train]  #prvih 70% izmjerenih vrijednosti su također dio trening skupa
xtest = x[indeksi_test] #drugih 30% indeksa su x os testnog skupa
ytest = y_measured[indeksi_test] #drugih 30% su također y os testnog skupa
linearModel = lm.LinearRegression() #definiranje linearnog modela preko linearne regresije
linearModel.fit(xtrain,ytrain) #linearni model se trenira preko xtrain i ytrain varijabli
ytest_p = linearModel.predict(xtest)
plt.plot(residuals(xtrain,ytrain,linearModel.intercept_,linearModel.coef_),xtrain,'o') #crtanje reziduala

