import numpy as np 
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures 
 
def non_func(x):     
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y 
 
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
    
x = np.linspace(1,10,50)
y_true = non_func(x)
y_measured = add_noise(y_true) 
 
x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis] 
 
# make polynomial features 

#racunanje polinoma 2., 6. i 15, stupnja 
poly15 = PolynomialFeatures(degree=15)
poly6=PolynomialFeatures(degree=6)
poly2=PolynomialFeatures(degree=2)
#fit-anje polinoma prema x vrijednostima
xnew15 = poly15.fit_transform(x)
xnew6=poly6.fit_transform(x)
xnew2=poly2.fit_transform(x)

np.random.seed(12)
indeksi = np.random.permutation(len(xnew15))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew15)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnew15)))+1:len(xnew15)] 
 
xtrain = xnew15[indeksi_train]
ytrain = y_measured[indeksi_train] 
 
xtest = xnew15[indeksi_test]
ytest = y_measured[indeksi_test] 
 
linearModel15 = lm.LinearRegression() #linearni regresivni model za polinom 15. reda
linearModel15.fit(xtrain,ytrain) #treniranje modela
linearModel6=lm.LinearRegression() #linearni regresivni model za polinom 6. reda
linearModel6.fit(xnew6[indeksi_train],ytrain) #treniranje modela
linearModel2=lm.LinearRegression() #linearni regresivni model za polinom 2. reda
linearModel2.fit(xnew2[indeksi_train],ytrain) #treniranje modela
 
#predikcija svakog linearnog modela nad test podatcima
ytest_p15 = linearModel15.predict(xtest)
ytest_p6=linearModel6.predict(xnew6[indeksi_test])
ytest_p2=linearModel2.predict(xnew2[indeksi_test])

#predikcija svakog lin. modela nad trening podatcima
ytrain_p15 = linearModel15.predict(xtrain)
ytrain_p6=linearModel6.predict(xnew6[indeksi_train])
ytrain_p2=linearModel2.predict(xnew2[indeksi_train])

#kvad. pogreska sva 3 modela nad testnim i trening podatcima
MSE_test=np.array([mean_squared_error(ytest,ytest_p2),mean_squared_error(ytest,ytest_p6),mean_squared_error(ytest,ytest_p15) ])
MSE_train=np.array([mean_squared_error(ytrain,ytrain_p2),mean_squared_error(ytrain,ytrain_p6),mean_squared_error(ytrain,ytrain_p15) ])

print "MSE_test[2 6 15]= ",MSE_test,"\nMSE_train[2 6 15]= ",MSE_train
plt.figure(1) #crtanje sva 3 modela
plt.plot(x,y_true,'c',label='f')
plt.plot(xtest[:,1],ytest_p15,'or',label='Degree=15')
plt.plot((xnew6[indeksi_test])[:,1],ytest_p6,'og',label='Degree=6')
plt.plot((xnew2[indeksi_test])[:,1],ytest_p2,'ob',label='Degree=2')
plt.plot(x, linearModel15.predict(xnew15),'r-',label='model15')
plt.plot(x, linearModel6.predict(xnew6),'g-',label='model6')
plt.plot(x, linearModel2.predict(xnew2),'b-',label='model2')

plt.legend(loc = 4) 
 