import numpy as np 
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures 
 
def non_func(x):     #stvarna funkcija
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y 
 
def add_noise(y): #racunaju se y vrijednosti na koje je dodan sum
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
    
x = np.linspace(1,10,50) #x sadrzi 50 ravnomjerno rasporedjenih vrijednosti od 1 do 10
y_true = non_func(x)
y_measured = add_noise(y_true) 
 
x = x[:, np.newaxis] #transponiranje vektora
y_measured = y_measured[:, np.newaxis]#transponiranje vektora
 
# make polynomial features 

poly = PolynomialFeatures(degree=15) #racunanje polinoma 15-og stupnja 
xnew = poly.fit_transform(x) #fit-anje polinoma prema x vrijednostima
np.random.seed(12)
indeksi = np.random.permutation(len(xnew)) #nasumicne vrijednosti od 1 do len(xnew)
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew)))] #70% tih vrijednosti su za treniranje
indeksi_test = indeksi[int(np.floor(0.7*len(xnew)))+1:len(xnew)]  #dok je 30% za testiranje
 
xtrain = xnew[indeksi_train] #izvlacenje xtrain vrijednosti iz xnew
ytrain = y_measured[indeksi_train]  #izvlacenje ytrain vrijednosti iz y_measured
 
xtest = xnew[indeksi_test] #izvlacenje xtest vrijednosti iz xnew
ytest = y_measured[indeksi_test]  #izvlacenje ytest vrijednosti iz y_measured
 
linearModel = lm.LinearRegression() #definiranje linearnog regresivnog modela
linearModel.fit(xtrain,ytrain) #treniranje modela
 
ytest_p = linearModel.predict(xtest) #podatci koje je model predvidio nad xtest podatcima
MSE_test = mean_squared_error(ytest, ytest_p)  #racunanje kvad. greske
 
plt.figure(1)
plt.plot(xtest[:,1],ytest_p,'og',label='predicted')
plt.plot(xtest[:,1],ytest,'or',label='test')
plt.legend(loc = 4) 
 
#pozadinska funkcija vs model
plt.figure(2)
plt.plot(x,y_true,label='f')
plt.plot(x, linearModel.predict(xnew),'r-',label='model')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(xtrain[:,1],ytrain,'ok',label='train')
plt.legend(loc = 4) 
 