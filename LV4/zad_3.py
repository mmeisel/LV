import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
def non_func(x): #funkcija koja vraca y listu koja sadrzi stvarne vrijednosti
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
def add_noise(y): #funkcija koja na osnovi y_true vraca stvarne vrijednosti na koje je dodan sum/pogreska
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
    
def gradientDecent(x,y): #racunaju se parametri gradijentnog spusta
    alpha=0.05 #alpha se uzima da je 0.05
    n=len(x) #n je broj clanova koji ce se koristiti za ucenje
    thetaJ=np.array([1,1]) #posto iz prijasnjih testiranja smo dobili vrijednosti [0,1] postavljamo [1,1]; moze se i veci brojevi ali ce onda biti potreban veci broj koraka uz isti alpha
    plt.figure(4)
    for i in range(2200):
        temp0=thetaJ[0]-(alpha*np.sum(thetaJ[0]+np.dot(thetaJ[1],x)-y))/n #racuna se odsjecak na y osi s obzirom na odstupanje i prijasnju vrijednost
        temp1=thetaJ[1]-(alpha*np.sum((thetaJ[0]+np.dot(thetaJ[1],xtrain)-ytrain)*xtrain))/n #racuna se koef. uz x s obzirom na odstupanje i prijasnju vrijednost
        thetaJ=np.array([temp0,temp1]) #stavljaju se vrijednosti u jedan vektor
        if i%8==0:
            plt.plot(temp0,temp1,'o') #crta se svaki 8. uzorak
    return thetaJ #vracaju se dobiveni koeficijenti theta0 i theta1

x = np.linspace(1,10,100)
y_true = non_func(x) #stvarna funkcija koja se treba dobiti sa SU
y_measured = add_noise(y_true) #gneriranje ulaznog signala koji se mjeri; signal je dobiven iz stvarne funkcije dodavanjem suma
plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno') #crtaju se uzorci dobiveni mjerenjem
plt.plot(x,y_true,label='stvarno') #crta se stvarna funkcije kako bi mogli procjeniti koliko dobro mjereni uzorci odgovaraju stvarnoj funkciji
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
np.random.seed(12)
indeksi = np.random.permutation(len(x)) #nasumicna permutacija brojeva do len(x)
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))] #prvih 70% indeksa su indeksi trening skupa
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)] #slijedecih 30% su indexi testnog skupa
x = x[:, np.newaxis] #transponiranje liste x koja je linspace
y_measured = y_measured[:, np.newaxis] #transponira listu mjerenja
xtrain = x[indeksi_train]           #iz varijable x izvlaci se prvih 70% mjerenja koje su trening skup
ytrain = y_measured[indeksi_train]  #prvih 70% izmjerenih vrijednosti su također dio trening skupa
xtest = x[indeksi_test] #drugih 30% indeksa su x os testnog skupa
ytest = y_measured[indeksi_test] #drugih 30% su također y os testnog skupa
plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train') #plave tocke na grafu predstavljaju trening skup
plt.plot(xtest,ytest,'or',label='test') #crvene tocke na grafu predstavljaju testni skup
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
linearModel = lm.LinearRegression() #definiranje linearnog modela preko linearne regresije
linearModel.fit(xtrain,ytrain) #linearni model se trenira preko xtrain i ytrain varijabli
print 'Model je oblika y_hat = Theta0 + Theta1 * x'
print 'y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x'
ytest_p = linearModel.predict(xtest) #izlazi na osnovi testnog x skupa
MSE_test = mean_squared_error(ytest, ytest_p) #racunanje srednje kvadratne pogreske
plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted') #izlazi koje je model dao na osnovi testnog skupa
plt.plot(xtest,ytest,'or',label='test') #stvarni izlazi

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac) #y vrijednost koju ce nam dati model na osnovi linspace vrijednosti
plt.plot(x_pravac, y_pravac) #crtanje funkcije po kojoj se prediđaju vrijednosti

print "Koeficijenti gradijentnog spusta:",gradientDecent(xtrain,ytrain)
