#Za mtcars skup podataka napišite programski kod koji će odgovoriti na sljedeća pitanja:
#1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
#2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
#3. Kolika je srednja potrošnja automobila sa 6 cilindara?
#4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
#5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
#6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
#7. Kolika je masa svakog automobila u kilogramima?

import pandas as pd
import numpy as np

mtcars=pd.read_csv('mtcars.csv') #ucitava se csv datoteka u dataframe mtcars

mtcars=mtcars.sort_values(['mpg'],ascending=0) #sortiraju se vrijednosti po potrosnji uzlazno
print 'Prvih 5 automobila s najvecom potrosnjom:\n'
print mtcars.head(5) #ispisuju se prvih 5 automobila a posto su sortirani tada su prvih 5 s najvecom potrosnjom

print 'Tri automobila sa 8 cilindara koji imaju najmanju potrosnju:\n'
print mtcars[mtcars.cyl==8].tail(3) #ispisuje se zadnja 3 automobila a posto su sortirani tada su zadnja 3 s najmanjom potrosnjom

avg6=np.average(mtcars[mtcars.cyl==6]['mpg']) #racuna se prosjek potrosnje auta sa 6 cilindara
print 'Prosjek potrosnje automobila sa 6 cilindara', avg6


avg4=np.average(mtcars[(mtcars.wt>=2.2) & (mtcars.wt<=2.4) & (mtcars.cyl==4)]['mpg']) #racuna se prosjek potrosnje auta sa 4 cilindra mase između 2000 i 2200 lbs
print 'Prosjek potrosnje automobila sa 4 cilindra mase izmedju 2000 i 2200 lbs', avg4

rucni=len(mtcars[mtcars.am==1]) #prebrojavaju se auti s rucnim mjenjacem
automatik=len(mtcars[mtcars.am==0]) #prebrojavaju se auti s automatskim mjenjacem
print 'Rucni mjenjac: ', rucni, 'automobila'
print 'Automatski mjenjac: ', automatik, 'automobila'

automatikHP=len(mtcars[(mtcars.am==0) & (mtcars.hp>100)]) #prebrojavaju se auti s automatskim mjenjacem snage iznad 100 ks
print 'Broj automobila s automatskim mjenjacem i preko 100 ks:',automatikHP

mtcars.wt=mtcars.wt*453.592 #lbs se pretvaraju u kg; wt=lbs/1000, 1lbs = 0.453592 -> stupac wt trebamo pomnoziti sa 453.592
print mtcars