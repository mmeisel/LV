#Na stranici http://iszz.azo.hr/iskzl/exc.htm moguće je dohvatiti podatke o 
#kvaliteti zraka za Republiku Hrvatsku. Podaci se mogu preuzeti korištenjem 
#RESTfull servisa u XML ili JSON obliku. Ovdje možete pronaći skriptu koja dohvaća 
#podatke te ih pohranjuje u odgovarajući DataFrame. Prepravite/nadopunite skriptu
# s programskim kodom kako bi dobili sljedeće rezultate:  
#1. Dohvaćanje mjerenja dnevne koncentracije lebdećih čestica PM10 za 2015. godinu 
#za grad Osijek. 
#2. Ispis tri datuma u godini kada je koncentracija PM10 bila najveća.
#3. Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca.
#4. Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i 
#jednog ljetnog mjeseca.
#5. Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica 
#tijekom vikenda. 
def izbroji():
    tempSt=[] #privremena lista u koju ce se spremati broj izostalih dana
    for x in xrange(12): #brojac od 0 do 11
        temp=0           #privremena varijabla
        for d in df['vrijeme']: #za svaku celiju u stupcu vrijeme
            if re.match(r'^2015-[0]{0,1}'+str(x+1)+r'-.*',str(d)): #regularnim izrazom se pretrazuje koliko ima mjerenja za određeni mjesec
                temp+=1 #broji se koliko je pronadjeno podudaranja
        if(x==0 or x==2 or x==4 or x==6 or x==7 or x==9 or x==11): #racuna se koliko dana je izostavljeno
            tempSt.append([x+1,31-temp])                            #ovisno o kojem se mjesecu radi
        elif (x==1):                                                #jer neki imaju 30, 31 i 28 dana
            tempSt.append([x+1,28-temp])                           #i redni broj mjeseca i broj izostavljenih dana se sprema u listu
        else:
            tempSt.append([x+1,30-temp])
    return pd.DataFrame(tempSt,columns=['Mjesec','Broj izostavljenih dana']) #napravljena lista se pretvara u DataFrame s 2 stupca te se salje kao povratna vrijednost
import urllib
import re
import pandas as pd
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=01.01.2015&vrijemeDo=31.12.2015'
airQualityHR = urllib.urlopen(url).read()
root = ET.fromstring(airQualityHR)
df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))
i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1
df.vrijeme = pd.to_datetime(df.vrijeme)
df.plot(y='mjerenje', x='vrijeme');
# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

df=df.sort_values('mjerenje',ascending=0) #sortira se dataframe po stupcu mjerenja uzlazno
print 'Najveca koncentracija PM10: '
print df.head(3) #ispisuju se prva 3 mjerenja a posto su sortirani tada se ispisuju prva 3 najveca mjerenja
izostali=izbroji() #poziva se funkcija izbroji koja ce vratiti DataFrame koji sadrzi izostale dane
izostali.plot(x='Mjesec', y='Broj izostavljenih dana', kind='bar') #crta se barplot izostalih dana po mjesecima
plt.suptitle('Broj izostavljenih dana mjerenja')
df[df.month.isin([7,12])].boxplot(column='mjerenje', by='month') #crta se boxplot stupca mjerenja 7. i 12. mjeseca
plt.suptitle('Distribucija mjeseca srpnja i prosinca')
df['vikend']=0 #dodaje se stupac vikend i inicijalizira na 0 sto predstavlja da nije vikend
df.vikend[df.dayOfweek>=5]=1 #za svaku subotu i nedjelju se stupac vikend stavlja na 1
df.boxplot(column='mjerenje', by='vikend') #crta se boxplot stupca mjerenja grupirana po stupcu vikend