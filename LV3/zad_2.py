#Napišite programski kod koji će iscrtati sljedeće slike:
#1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
#2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine 
#automobila s 4, 6 i 8 cilindara.
#3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li
# automobili s ručnim mjenjačem veću
#potrošnju od automobila s automatskim mjenjačem?
#4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile 
#s ručnim odnosno automatskim
#mjenjačem.

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars=pd.read_csv('mtcars.csv')  #ucitava se csv datoteka u dataframe mtcars
temp=pd.DataFrame([[4,np.average(mtcars.mpg[mtcars.cyl==4])],[6,np.average(mtcars.mpg[mtcars.cyl==6])],[8,np.average(mtcars.mpg[mtcars.cyl==8])]],columns=['cyl','avg']) #racuna se dataframe s prosjecim vrijednostima za svaki cilindar
temp.plot(x='cyl',y='avg',kind='bar') #crta se bar graf dataframe-a temp
mtcars.boxplot(column = ['mpg' ,'wt'], by='cyl') #crta se boxplot stupaca potrosnje i tezine grupirani po broju cilindara
plt.suptitle('Distribucija potrosnje i tezine automobila', fontsize=12) #naslov grafa

mtcars.boxplot(column= ['mpg'], by=['am']) #crta se box plot stupca potrosnje grupirani po vrsti mjenjaca
print 'Automobili s automatskim mjenjaecm imaju manju potrosnju'

mtcars['qsVhp']=mtcars.qsec/mtcars.hp
mtcars.boxplot(column= 'qsVhp', by='am') #crta se box plot stupaca snage i ubrzanja grupirani po vrsti mjenjaca