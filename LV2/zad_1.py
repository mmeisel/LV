#Napišite Python skriptu koja će učitati tekstualnu datoteku, pronaći valjane email 
#adrese te izdvojiti samo prvi dio adrese (dio ispred znaka @). Koristite odgovarajući 
#regularni izraz. Koristite datoteku mbox-short.txt. Ispišite rezultat. 

import re
 
datoteka=open('mbox-short.txt')  #otvara se datoteka
useri=[]                         #kreira se lista useri
for linija in datoteka:          #za svaku liniju unutar datoteke
    adrese=re.findall(r'[a-zA-Z0-9][a-zA-Z0-9._]*@[a-zA-Z]+\.[a-zA-Z]+',linija) #pronađi sve dijelove koji odgovaraju regularnom izrazu
    if len(adrese)>0:    #ukoliko adresa ima vise od 0 znakova
        temp = re.split("@",adrese[0]) #razdvoji korisnicko ime na znaku @
        if temp[0] not in useri:        #ukoliko to korisnicko ime jos nije bilo zapisano u memoriju
            useri.append(temp[0])       #zapisi ga
 
print useri     #ispisuje sva pronađena korisnicka imena bez duplikata