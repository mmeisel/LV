#Napišite program koji će pomoću biblioteke Beautiful Soup ispisati 
#sve adrese i nazive za vanjske linkove web stranice koju definira korisnik.
#Npr. za stranicu www.etfos.unios.hr vanjski linkovi su linkovi koji 
#ne sadrže 'etfos.unios.hr' u svom URL-u niti pokazuju na neki od dokumenata 
#u poddirektorijima (npr. '/znanost/znanstveno-strucna-predavanja/'). 

import re
import urllib
from bs4 import BeautifulSoup

def ripDomain(): #funkcija koja "cupa" domenu iz linka
    if re.search(r'[w]{3}', urlAddr): #ukoliko link sadrzi www
        temp1=(urlAddr.split('www.')[1]).split('/')[0] #tada se dijeli link na znakovima "www." te se nakon toga dijeli na znaku /
    else: #u suprotnom
        temp1=(urlAddr.split('://')[1]).split('/')[0] #link pocinje sa http(s):// pa se dijeli na znakovima :// pa onda na znaku /
    return temp1 #vraca se domena
    
def correctURL(): #funkcija koja dodaje http:// ukoliko ga link ne sadrzi
    if not re.match(r'^http[s]{0,1}',urlAddr): #trazi se pocinje li link sa http(s)
        return ''.join(['http://', urlAddr]) #ukoliko ne pocinje tada se na pocetak dodaje http://
    else:
        return urlAddr #u suprotnom se vraca ne promjenjen link
    
urlAddr=raw_input('Upisite web lokaciju: ') #upisuje se web lokacija
urlAddr=correctURL()                        #ispravlja se url ukoliko je potrebno
html=urllib.urlopen(urlAddr, "lxml").read() #otvara se url
soup=BeautifulSoup(html)                    #i deklarira objekt tipa BeautifulSoup

domain=ripDomain()                          #trazi se domena
print 'Domena je: ',domain                  #i ispisuje se
tagovi=soup('a')                            #traze se svi tagovi 'a'
for tag in tagovi:                          #i za svaki pronađeni tag
    temp=re.search(r'http[s]{0,1}', str(tag.get('href'))) #provjerava se sadrzili http ili https
    temp1=re.search(domain, str(tag.get('href'))) #i sadrzi li domenu
    if temp and not temp1: #ukoliko sadrzi http(s) a ne sadrzi domenu tada se ispisuje na nacin
        if len(tag.contents)>0:  #ukoliko tag posjeduje atribut contents
            tagCont=''.join([unicode(tag.contents[0]),': ']) #tada spoji atribut sa znakom :
        else:
            tagCont='N/A: ' #ukoliko atribut nije pronađen onda se za naziv ispisuje N/A:
        if re.search(r'[?]http[s]{0,1}', str(tag.get('href'))): #ukoliko prije http(s)-a postoji niz znakova
            print tagCont, str(tag.get('href')).split('?')[1] #tada se oni uklanjaju i ispisuje se link
        else:
            print tagCont,tag.get('href') #ukoliko ne postoji nikakav znak tada se ispisuje lik