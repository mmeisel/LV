#Na stranici http://wiki.stat.ucla.edu/socr/index.php/SOCR_Data_MLB_HeightsWeights 
#nalazi se tablica s igračima MLB (Major League Baseball). Dohvatite podatke pomoću 
#BeautifulSoup. Prikažite težinu igrača s obzirom na poziciju. Načinite drugi graf 
#gdje s prikazom odnosa težine i visine igrača, ali na grafu ujedno i prikažite
# poziciju igrača. 

import urllib
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup
 
name=[]
team=[]
posit=[]
height=[]
weight=[]
age=[]

urlAddr="http://wiki.stat.ucla.edu/socr/index.php/SOCR_Data_MLB_HeightsWeights"
html=urllib.urlopen(urlAddr).read() #otvara se gore navedeni URL za citanje

soup=BeautifulSoup(html,"html.parser") #definira se objekt tipa BeautifulSoup
table=soup.find('table', { "class" : "wikitable" }) #preko objekta soup pronalazimo tablicu na linku klase wikitable
rows=table.findAll('tr') #pronalazimo sve tagove 'tr' koji predstavljaju retke tablice
for row in rows[1:]: #preskace se prvi redak jer sadrzi zaglavlje koje nam ne treba
    cols=row.findAll('td') #pronalaze se svi tagovi 'td' u redku koji predstavljaju stpce
    col1 = [ele.text.strip().replace(',','') for ele in cols] #iz tog redka se cjepkaju svi podatci na zasebne odvojeni znakom ,
    name.append(map(str,col1[0:1])) #pronađeno ime se upisuje u listu name
    team.append(map(str,col1[1:2])) #pronađeni tim se upisuje u listu team
    posit.append(map(str,col1[2:3]))#pronađena pozicija se upisuje u listu posit  
    height.append(map(int,col1[3:4])) #pronađena visina se upisuje u listu height
    try: #u jednom od redaka igrac nema vrijednost pa kako ne bi doslo do terminalne greske
        weight.append(map(int,col1[4:5])) #sve vrijednosti se upisuju u varijablu weight u try blocku
    except Exception: #kada program dođe do igraca koji nema vrijednost
        weight.append([180]) #upisuje se vrijednost 180 koja je otprilike prosjek svih igraca
    age.append(map(float, col1[5:6])) #pronađena dob igraca se upisuje u varijablu age

plt.figure(figsize=(10,8))
plt.plot(height,weight,'o') #crta se graf oisnosti tezine o visini
plt.figure(figsize=(10,8))
for a in xrange(len(name)): #for petlja preko koje se crta ovisnost tezine o visini
    if posit[a]==['Catcher']: #ali simbol i njegova boja ovise o poziciji igraca
        plt.plot(height[a],weight[a],'s', color='b')
    elif posit[a]==['First_Baseman']:
        plt.plot(height[a],weight[a],'o', color='g')
    elif posit[a]==['Second_Baseman']:
        plt.plot(height[a],weight[a],'D', color='k')
    elif posit[a]==['Third_Baseman']:
        plt.plot(height[a],weight[a],'x', color='c')
    elif posit[a]==['Shortstop']:
        plt.plot(height[a],weight[a],'+', color='m')
    elif posit[a]==['Designated_Hitter']:
        plt.plot(height[a],weight[a],'H', color='y')
    elif posit[a]==['Starting_Pitcher']:
        plt.plot(height[a],weight[a],'^', color='r')
    elif posit[a]==['Relief_Pitcher']:
        plt.plot(height[a],weight[a],'p', color='0.2')
    else:
        plt.plot(height[a],weight[a],'*', color='0.75')
    