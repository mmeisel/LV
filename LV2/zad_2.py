#Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane cjelobrojne
#vrijednosti 0 ili 1. Neka 1 označava mušku osobu, a 0 žensku osobu. Napravite drugi
#vektor koji sadrži visine osoba koje se dobiju uzorkovanjem odgovarajuće distribucije.
#U slučaju muških osoba neka je to Gaussova distribucija sa srednjom vrijednošću 180 cm
#i standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova distribucija
#sa srednjom vrijednošću 167 cm i standardnom devijacijom 7 cm. Prikažite podatke te ih
#obojite plavom (1) odnosno crvenom bojom (0). Napišite funkciju koja računa srednju 
#vrijednost visine za muškarce odnosno žene (probajte izbjeći for petlju pomoću 
#funkcije np.dot). Prikažite i dobivene srednje vrijednosti na grafu. 

                        
import numpy as np
import matplotlib.pyplot as plt

def dodjeliVisine():
    for x in xrange(len(spol)): #za svaku osobu ce se napraviti nesto
        if spol[x]==0: #ukoiko je spol jednak 0 
            visina[x]=np.random.normal(167,7) #onda ce se dodijeliti broj iz distribucije za zene
        else: #u suprotnom
            visina[x]=np.random.normal(180,7) #ce se dodijeliti broj iz distribucije za muskarce
            
def pros():
    stupac1=np.transpose(np.ones(a)) #definira se stupac koji sadrzi sve vrijednosti 1. Broj redaka je koliko ima zenskih osoba
    prosVisina=np.dot(visina[0:a],stupac1) #racuna se suma svih zenskih osoba
    prosVisina/=a #racuna se prosjecna visina
    plt.plot((0,a-1),(prosVisina,prosVisina),'m-') #i crta se na grafu u rozoj tj. magenta boji
    
    stupac1=np.transpose(np.ones(b)) #definira se stupac koji sadrzi sve vrijednosti 1. Broj redaka je koliko ima muskih osoba
    prosVisina=np.dot(visina[a:len(spol)],stupac1) #racuna se suma svih muskih osoba
    prosVisina/=b #racuna se prosjecna visina
    plt.plot((0,b-1),(prosVisina,prosVisina),'k-')#i crta se na grafu u crnoj boji
            
spol=np.random.randint(2,size=np.random.randint(50,100)) #definira se varijabla spol koja ce imati između 50 i 100 clanova te ce svaki clan moci imati vrijednos 0 ili 1
spol.sort() #sortira se varijabla spol
visina=np.zeros(len(spol)) #definira se varijabla visina koja ima zapisanu visinu svakog clana iz varijable spol
dodjeliVisine() #poziva se funkcija dodjeliVisine() koja ce za svakog clana iz spol dodijeliti vrijednost visine u varijablu visina
a=len(spol[spol==0]) #dohvaca koliko ima zapisa sa vrijednosti 0 i sprema u varijablu a
x=np.linspace(0,a-1,num=a) #definira se linearno mjerilo na grafu od 0 do a-1
plt.plot(x,visina[0:a],'r-') #crtaju se vrijednosti u crvenoj boji

b=len(spol[spol==1]) #dohvaca se koliko ima zapisa sa vrijednosti 1 i sprema u varijablu b
x=np.linspace(0,b-1,num=b) #definira se ponovno linearno mjerilo na grafu od 0 do b-1
plt.plot(x,visina[a:len(visina)],'b-') #crtaju se vrijednosti u plavoj boji
pros() #poziva se funkcija pros()

plt.ylabel("Visina [cm]") #naziv y osi
plt.xlabel("Broj mjerenja") #naziv x osi
plt.title("Ovisnost visine o spolu") #naslov grafa
print "Broj generiranih muskih osoba: ",b #ispisuje se broj generiranih muskih i zenskih osoba
print "Broj generiranih zenskih osoba:",a #te zajedno koliko je ukupno generirano osoba
print "Ukupno generirano:             ",a+b