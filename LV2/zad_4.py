#Simulirajte bacanje igraće kocke 30 puta. Izračunajte srednju vrijednost rezultata 
#i standardnu devijaciju. Pokus ponovite 1000 puta. Prikažite razdiobu dobivenih
#srednjih vrijednosti. Što primjećujete? Kolika je srednja vrijednost i standardna 
#devijacija dobivene razdiobe? Što se događa s ovom razdiobom ako pokus 
#ponovite 10000 puta? 

import numpy as np
import matplotlib.pyplot as plt
v=input('Koliko puta zelite pokus ponoviti: ')
avg=np.zeros(v) #definira se prosjecni vektor velicine v
stdDev=np.zeros(v) #definira se vektor std. devijacije velicine v
for x in range(v): #za svako ponavljanje pokusa
    kockica=np.random.randint(1,7,size=30) #generira se 30 nasumicnih cijelih brojeva iz intervala [1,7>
    avg[x]=np.average(kockica) #racuna se prosjek od tih 30 brojeva
    stdDev[x]=np.std(kockica) #i standardna devijacija
plt.hist(avg,normed=1) #crta se histogram prosjecnih vrijednosti
plt.hist(stdDev,normed=1) #i histogram standardne devijacije