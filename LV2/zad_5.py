#Git repozitorij https://gitlab.com/rgrbic/data_LV2.git sadrži 
#datoteku mtcars.csv koja sadrži različita mjerenja provedena na 32 
#automobila (modeli 1973-74). Prikažite ovisnost potrošnje automobila (mpg) o 
#konjskim snagama (hp). Na istom grafu prikažite i informaciju o težini pojedinog 
#vozila. 

import csv
import numpy as np
import matplotlib.pyplot as plt

mpghp=np.zeros((32,3)) #definira se matrica 32x3
nameW=[] #definira se lista nameW
with open('mtcars.csv', 'rb') as f: #otvara se mtcars.csv za citanje
    reader = csv.reader(f) 
    next(reader,None) #preskace se zaglavlje datoteke
    redakX=0 #broje se redci
    for redak in reader: #za svaki redak koji citac procita
        mpghp[redakX]=[float(redak[1]), float(redak[4]), float(redak[6])] #sprema se u matricu
        nameW.append(redak[0]) #te se naziv automobila također sprema u listu
        redakX+=1 #uvecava se

for x in xrange(len(mpghp)):
    sizeD=np.round(mpghp[x,2]*7)
    plt.plot(mpghp[x,0],mpghp[x,1],'.',ms=sizeD, color='b')