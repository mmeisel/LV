#Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6). 
#Pomoću histograma prikažite rezultat ovih bacanja. 

import numpy as np
import matplotlib.pyplot as plt

kocka=np.random.randint(1,7,size=100) #definira se 100 nasumicnih cijelih brojeva između [1,7>

plt.hist(kocka, bins=11, normed=1) #crta se normirani histogram varijable kocka
