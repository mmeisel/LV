#Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja
# nekakvu ocjenu i nalazi se između 0.0 i 1.0. Ispišite kojoj kategoriji 
# pripada ocjena na temelju sljedećih uvjeta: 
# 
#>= 0.9 A 
#>= 0.8 B 
#>= 0.7 C 
#>= 0.6 D 
#< 0.6 F

try:                                    #try blok ako korisnik upise nebrojcanu vrijednost
    ocjena=input("Upisite ocjenu: ")    #trazi se unos od korisnika
    if ocjena>=0.0 and ocjena<=1.0:     #provjerava se jeli uneseni broj u rasponu od 0 i 1
        if ocjena>=0.9:                 #ukoliko je, ispisuje se ocjena zavisno o unosu
            print "A"
        elif ocjena>=0.8:
            print "B"
        elif ocjena>=0.7:
            print "C"
        elif ocjena>=0.6:
            print "D"
        else:
            print "F"
    else:
        print "Uneseni raspon nije unutar intervala [0.0,1.0]"  #ukoliko nije u rasponu, ispisat ce se odgovarajuca poruka
except:                                 #ukoliko je upisana ne brojcana vrijednost, izvodi se
    print "Niste unjeli brojku"         #print koji ispisuje da nije unesena brojka
