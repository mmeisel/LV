# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 22:36:34 2016

@author: marko
"""
#Napišite program koji od korisnika zahtijeva unos imena tekstualne datoteke. 
#Program nakon toga treba tražiti linije oblika: 
# 
#X-DSPAM-Confidence: <neki_broj> 
# 
#koje predstavljaju pouzdanost korištenog spam filtra. Potrebno je izračunati 
#srednju vrijednost pouzdanosti. Koristite datoteke mbox.txt i mbox-short.txt 
 


def confidence():                                       #deklaracija funkcije confidence()
    zbroj=0                                             #inicijalizacija i deklaracija potrebnih varijabli
    broj=0                          
    for linija in datoteka:                             #za svaku liniju u datoteci
        if linija.startswith('X-DSPAM-Confidence:'):    #pronaci ce redak koji pocinje sa X-DSPAM-Confidence:
            temp=linija.split();                        #napravit ce listu koja ce imati dva clana razdvojena na razmaku poslije znaka ":", temp[0] ce biti X-DSPAM-Confidence: a temp[1] vrijednost koja nam je potrebna
            zbroj+=float(temp[1])                       #pribraja se temp[1] i casta u float
            broj+=1                                     #brojac pronadjenih brojeva
        
    print "Ime datoteke: ", datoteka.name               #ispisuje se ime datoteke koja je trenutno otvorena
    print "Average X-DSPAM-Confidence: ", zbroj/broj    #ispisuje se srednja vrijednost

datoteka=open('mbox.txt')                               #otvara se datoteka mbox.txt
confidence()                                            #poziva se funkcija confidence koja ce odraditi potrebne naredbe za izracun
datoteka.close()                                        #zatvara se datoteka

datoteka=open('mbox-short.txt')
confidence()
datoteka.close()
