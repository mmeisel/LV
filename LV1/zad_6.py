# -*- coding: utf-8 -*-
"""
Created on Wed Oct 26 00:04:55 2016

@author: marko
"""
#Napišite Python skriptu koja će učitati tekstualnu datoteku te 
#iz redova koji počinju s „From“ izdvojiti mail adresu te ju spremiti 
#u listu. Nadalje potrebno je napraviti dictionary koji sadrži hostname 
#(dio emaila iza znaka @) te koliko puta se pojavljuje svaki hostname u
# učitanoj datoteci. Koristite datoteku mbox-short.txt. Na ekran ispišite
# samo nekoliko email adresa te nekoliko zapisa u dictionary-u. 
 
 

adrese=[]                               #inicijalizira se lista adrese
rijecnik=dict()                         #i dictionary rijecnik
datoteka=open("mbox-short.txt")         #otvara se datoteka
for linija in datoteka:                 #za svaku liniju u datoteci
    if linija.startswith("From:"):      #ukoliko linija zapocinje sa From:
        temp=linija.split()             #razdvojit ce string na razmaku poslije From: i spremiti u temp[0]="From:" a u temp[1] trazenu adresu
        adrese.append(temp[1])          #dodat ce u listu adresa temp[1] koja sadrzi nadjenu adresu
        temp2=temp[1].split("@")        #kako bi dodali u rijecnik, razdvajamo adresu na znaku @ i u temp2[0] bit ce sadrzavana adresa prije @ a u temp2[1] hostname
        if temp2[1] not in rijecnik:    #ukoliko hostname nije u rijecniku
            rijecnik[temp2[1]] = 1      #dodat ce se
        else:                           #ukoliko je, uvecat ce se vrijednost za 1
            rijecnik[temp2[1]] += 1

print adrese[0:3]                       #ispis prve 3 adrese
i=3
for keys, values in rijecnik.items():   #ispis prva 3 para kljuca-vrijednosti iz rijecnika
    print repr(keys)+" : "+repr(values)
    i-=1
    if i==0:
        break