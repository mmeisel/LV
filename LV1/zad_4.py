#Napišite program koji od korisnika zahtijeva unos brojeva u beskonačnoj petlji
# sve dok korisnik ne upiše „Done“ (bez navodnika). Nakon toga potrebno je 
#ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i 
#maksimalnu vrijednost. Osigurajte program od krivog unosa (npr. slovo 
#umjesto brojke) na način da program zanemari taj unos i ispiše odgovarajuću poruku. 
 
 

brojUnesenih=0                                      #deklaracija varijabli
sredina=0.0
minBroj=None
maxBroj=None
while 1:                                            #beskonacna petlja
    try:                                            #try-except blok ukoliko korisnik unese nebrojcanu vrijednost a da nije Done
        unos=raw_input("Unesite broj\n")            #raw_input svaki unos sprema kao string i uklanja znak novog reda
        if unos == "Done":                          #ukoliko je korisnik unio Done, prekida se petlja
            break
        else:                                       #ukoliko nije unio done
            brojUnesenih+=1                         #povecava se brojac unesenih brojeva
            tempUnos=int(unos)                      #posto raw_input je unos spremio kao string, taj input se mora castati/pretvoriti u integer kako bi se mogla raditi usporedba
            if minBroj>tempUnos or minBroj==None:   #postavlja trenutno unesen broj kao najmanji broj ukoliko prvi puta unosimo ili ako je unesen broj manji od spremljenog najmanjeg broja
                minBroj=tempUnos
            if maxBroj<tempUnos or maxBroj==None:   #postavlja trenutno unesen broj kao najveci broj ukoliko prvi puta unosimo ili ako je unesen broj manji od spremljenog najveceg broja
                maxBroj=tempUnos
            sredina=sredina+tempUnos                #svi uneseni brojevi se zbrajaju

    except Exception:
        print "Greska tokom upisa"
if brojUnesenih!=0:                  #ukoliko je korisnik unio nebrojcanu vrijednost a da nije done ispisat ce se poruka
    sredina=sredina/float(brojUnesenih)                 #racuna se srednja vrijednost unesenih brojeva
    print "Srednji broj: %f" % sredina                  #ispis srednjeg, najveceg i najmanjeg broja te broj unesenih brojeva
    print "Najveci broj: %d" % maxBroj
    print "Najmanji broj: %d" % minBroj
    print "Broj unesenih: %d" % brojUnesenih
else:
    print "Doslo je do pogreske"
