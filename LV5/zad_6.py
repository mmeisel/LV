import matplotlib.pyplot as plt
import sklearn.metrics as skm
import sklearn.linear_model as lm
import numpy as np

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest') 
 
    width = len(c_matrix)
    height = len(c_matrix[0]) 
 
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)
 
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show() 

            
def generate_data(n):

    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data = np.concatenate((temp1,temp2),axis = 0)
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    
    return data

def evaluateModel(cM):
    #racunanje pokazatelja klas. modela
    suma=np.sum(c_matrix)
    temp=np.array([float(cM[0,0]+cM[1,1])/suma,
                   1-float(cM[0,0]+cM[1,1])/suma,
                    float(cM[0,0])/(np.sum(cM[0])),
                    float(cM[0,0])/(np.sum(cM[:,0])),
                    float(cM[1,1])/(np.sum(cM[:,1]))
                   ])
    return temp*100

#generiranje trening podataka
np.random.seed(12)
xtrain=generate_data(200)
#generiranje test podataka
np.random.seed(12)
xtest=generate_data(100)


#deklaracija modela i "fittanje"
logRegress=lm.LogisticRegression()
logRegress.fit(xtrain[:,0:2],xtrain[:,2])
ypred=logRegress.predict(xtest[:,0:2])
#racuanje matrice zabine
c_matrix=skm.confusion_matrix(xtest[:,2],ypred)

ParamEval=evaluateModel(c_matrix)
print "Tocnost: ", ParamEval[0], "%"
print "Ucestalost pogresne klasifikacije: " , ParamEval[1], "%"
print "Preciznost: ",  ParamEval[2], "%"
print "Odziv: ",  ParamEval[3], "%"
print "Specificnost: ",  ParamEval[4], "%"

plot_confusion_matrix(c_matrix)