import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import confusion_matrix


def generate_data(n):

    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data = np.concatenate((temp1,temp2),axis = 0)
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    
    return data
def drawData():
    plt.scatter(xtrain[:,0][xtrain[:,2]==0], xtrain[:,1][xtrain[:,2]==0], c='r')
    plt.scatter(xtrain[:,0][xtrain[:,2]==1], xtrain[:,1][xtrain[:,2]==1], c='b')
    
def drawGraph():
    #crtanje grafa ovisno o "predvidjenim" podatcima
    plt.scatter(xtrain[:,0][xtrain[:,2]==classPredicted], xtrain[:,1][xtrain[:,2]==classPredicted], c='g')
    plt.scatter(xtrain[:,0][xtrain[:,2]!=classPredicted], xtrain[:,1][xtrain[:,2]!=classPredicted], c='y')
    plt.xlabel("$x_1$")
    plt.ylabel("$x_2$")
    plt.title("Predvidjeni podatci\nZuti - lose predvidjeni\nZeleni - dobro predvidjeni")
    
    
def drawDecisionCurve():
    h=0.02
    x_min, x_max = xtrain[:, 0].min() - 1, xtrain[:, 0].max() + 1
    y_min, y_max = xtrain[:, 1].min() - 1, xtrain[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    coeff=poly.fit_transform(np.c_[xx.ravel(),yy.ravel()])

    # testiranje svakog dijela kreiranog mesha
    Z = logRegress.predict(coeff)

    # crtanje dobivene granice odluke
    Z = Z.reshape(xx.shape)
    drawData()
    plt.contour(xx, yy, Z, cmap=plt.cm.Paired)
    plt.xlabel("$x_1$")
    plt.ylabel("$x_2$")
    plt.title("Krivulja odluke i testni podatci")

def drawProbability():
    f, ax = plt.subplots(figsize=(8, 6))
    x_grid, y_grid = np.mgrid[min(xtrain[:,0])-0.5:max(xtrain[:,0])+0.5:.05,
                              min(xtrain[:,1])-0.5:max(xtrain[:,1])+0.5:.05]
    
    grid = poly.fit_transform(np.c_[x_grid.ravel(), y_grid.ravel()])
    probs = logRegress.predict_proba(grid)[:, 1].reshape(x_grid.shape) 
     
    cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="afmhot", vmin=0, vmax=1) 
     
    ax_c = f.colorbar(cont)
    ax_c.set_label("$P(y = 1|\mathbf{x})$")
    ax_c.set_ticks([0, .25, .5, .75, 1])
    ax.set_xlabel('$x_1$', alpha=0.9)
    ax.set_ylabel('$x_2$', alpha=0.9)
    ax.set_title('Izlaz logisticke regresije')
    drawData()
    plt.show()
    
def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest') 
 
    width = len(c_matrix)
    height = len(c_matrix[0]) 
 
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)
 
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show() 

def evaluateModel(cM):
    #racunanje pokazatelja klas. modela
    suma=np.sum(c_matrix)
    temp=np.array([float(cM[0,0]+cM[1,1])/suma,
                   1-float(cM[0,0]+cM[1,1])/suma,
                    float(cM[0,0])/(np.sum(cM[0])),
                    float(cM[0,0])/(np.sum(cM[:,0])),
                    float(cM[1,1])/(np.sum(cM[:,1]))
                   ])
    return temp*100
#generiranje trening podataka
np.random.seed(12)
xtrain=generate_data(200)
#generiranje test podataka
np.random.seed(12)
xtest=generate_data(100)
poly=PolynomialFeatures(degree=2,include_bias=False)
polyTrain=poly.fit_transform(xtrain[:,0:2])

#deklariranje modela log. regresije i "fittanje"
logRegress=lm.LogisticRegression()
logRegress.fit(polyTrain,xtrain[:,2])

drawDecisionCurve()
drawProbability()
  
#predvidjanje modela
classPredicted=logRegress.predict(polyTrain)
drawGraph()

c_matrix=confusion_matrix(xtrain[:,2],classPredicted)
plot_confusion_matrix(c_matrix)

ParamEval=evaluateModel(c_matrix)
print "Tocnost: ", ParamEval[0], "%"
print "Ucestalost pogresne klasifikacije: " , ParamEval[1], "%"
print "Preciznost: ",  ParamEval[2], "%"
print "Odziv: ",  ParamEval[3], "%"
print "Specificnost: ",  ParamEval[4], "%"